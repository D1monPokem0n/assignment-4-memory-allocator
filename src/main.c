#include "mem.h"
#include "mem_internals.h"
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#define HEAP_SIZE 1024

static struct block_header *block_get_header(void *contents)
{
    return (struct block_header *)(((uint8_t *)contents) - offsetof(struct block_header, contents));
}

static void test_heap_init()
{
    void *heap = heap_init(HEAP_SIZE);
    assert(heap != NULL);

    struct block_header *header = heap;
    assert(header->capacity.bytes == capacity_from_size((block_size){8 * HEAP_SIZE}).bytes);
    assert(header->is_free == true);
    assert(header->next == NULL);

    heap_term();
}

#define BLOCK_SIZE 128
static void test_malloc_one_block()
{
    void *heap = heap_init(HEAP_SIZE);
    debug_heap(stdout, heap);

    void *block = _malloc(BLOCK_SIZE);
    assert(block != NULL);

    struct block_header *header = block_get_header(block);
    assert(header == heap);
    assert(header->capacity.bytes == BLOCK_SIZE);
    assert(header->is_free == false);
    assert(header->next != NULL);

    _free(block);
    assert(header->is_free == true);
    heap_term();
}

static void test_free_one_block()
{
    debug_heap(stdout, heap_init(HEAP_SIZE));
    void *block1 = _malloc(BLOCK_SIZE);

    struct block_header *header1 = block_get_header(block1);
    void *block2 = _malloc(BLOCK_SIZE);
    struct block_header *header2 = block_get_header(block2);
    void *block3 = _malloc(BLOCK_SIZE);
    struct block_header *header3 = block_get_header(block3);
    assert(block1 != NULL);
    assert(block2 != NULL);
    assert(block3 != NULL);

    _free(block2);

    assert(header1->is_free == false);
    assert(header2->is_free == true);
    assert(header3->is_free == false);

    heap_term();
}

static void test_malloc_block_with_new_region()
{
    debug_heap(stdout, heap_init(HEAP_SIZE));
    _malloc(BLOCK_SIZE);
    _malloc(BLOCK_SIZE);
    _malloc(BLOCK_SIZE);

    _malloc(HEAP_SIZE / 2);
    void *block = _malloc(HEAP_SIZE / 2);

    assert(block != NULL);
    assert(block_get_header(block)->capacity.bytes == HEAP_SIZE / 2);
    heap_term();
}

int main()
{
    test_heap_init();
    printf("test_heap_init: Success\n");
    test_malloc_one_block();
    printf("test_malloc_one_block: Success\n");
    test_free_one_block();
    printf("test_free_one_block: Success\n");
    test_malloc_block_with_new_region();
    printf("test_malloc_block_with_new_region: Success\n");
    printf("\nSuccess\n");
    return 0;
}